//
//  InformationMock.swift
//  Capitulo6-SwiftUI
//
//  Created by Salvador Lopez on 26/05/23.
//

import SwiftUI

struct Item: Identifiable{
    let id = UUID()
    let name: String
    let description: String
}
