//
//  Capitulo6_SwiftUIApp.swift
//  Capitulo6-SwiftUI
//
//  Created by Salvador Lopez on 26/05/23.
//

import SwiftUI

@main
struct Capitulo6_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ImageContentView()
        }
    }
}
