//
//  DetailView.swift
//  Capitulo6-SwiftUI
//
//  Created by Salvador Lopez on 26/05/23.
//

import SwiftUI

struct DetailView: View{
    let item: Item
    
    var body: some View{
        VStack{
            Text(item.name)
                .font(.largeTitle)
            Text(item.description)
                .padding()
            Text("\(item.id)")
                .font(.subheadline)
        }
    }
}
