//
//  ContentView.swift
//  Capitulo6-SwiftUI
//
//  Created by Salvador Lopez on 26/05/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Image(systemName: "globe")
                .imageScale(.large)
                .foregroundColor(.accentColor)
            Text("Hello, world!")
                .font(.title)
                .fontWeight(.regular)
                .foregroundColor(.orange)
        }
        .padding()
    }
}

struct TextContentView: View{
    var body: some View{
        VStack{
            Text("Hola")
                .font(.largeTitle)
            Text("Mundo")
                .font(.title)
            Text("!!")
                .font(.title2)
        }
    }
}

struct ButtonContentView: View{
    var body: some View{
        Button {
            print("ouch!!")
        } label: {
            Text("PRESIONAME")
                .frame(width: 200, alignment: .center)
                .font(.headline)
                .foregroundColor(.white)
                .padding()
                .background(Color.blue)
                .cornerRadius(50)
        }
    }
}

struct ImageContentView: View{
    var body: some View{
        ZStack {
            Color.blue
                .edgesIgnoringSafeArea(.all)
            Image("tux")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .frame(width: 200,height: 200)
                .background(Color.red)
                .cornerRadius(100)
        }
    }
}

struct TextFieldContentView: View{
    
    @State private var texto: String = ""
    
    var body: some View{
        VStack{
            TextField("Ingresa un texto", text: $texto)
                .textFieldStyle(.roundedBorder)
                .padding()
            Button("Imprime el texto"){
                print(texto)
            }
            Text("El texto ingresado es: \(texto)")
                .foregroundColor(.gray)
        }
    }
}

struct ToggleContentView: View{
    
    @State var estadoToggle = false
    
    var body: some View{
        VStack{
            Toggle(isOn: $estadoToggle) {
                Text("Activado/Desactivado")
            }
            .padding()
            Text("El estado del Toggle es: \(estadoToggle ? "Activo" : "Desactivado")")
                .padding()
        }
    }
}

struct SliderContentView: View{
    
    @State private var fontSize: CGFloat =  20.0
    
    var body: some View{
        VStack{
            Text("¡cambia el tamaño de la fuente!")
                .font(.system(size: fontSize))
                .padding()
            Slider(value: $fontSize, in: 10...50, step: 1)
                .padding()
        }
    }
}

struct StepperContentView: View{
    
    @State var itemCount: Int = 0
    
    var body: some View{
        VStack{
            
            Text("Carrito de compra")
                .font(.title)
                .padding(.top, 20)
            
            Spacer()
            
            Image(systemName: "cart")
                .resizable()
                .frame(width: 100,height: 100,alignment: .center)
            
            Spacer()
            
            Text("Cantidad: \(itemCount)")
                .font(.headline)
            
            Stepper {
                Text("Agregar al carrito")
            } onIncrement: {
                self.itemCount += 1
            } onDecrement: {
                if self.itemCount > 0 {
                    self.itemCount -= 1
                }
            }
            .padding()
            
            Button(action: {
                // Realizar compra
            }){
                Text("Comprar")
                    .foregroundColor(.white)
            }
            .padding()
            .background(Color.green)
            .cornerRadius(10)
            
            Spacer()

        }
    }
}


struct ListContentView: View{
    
    let items = [
        Item(name: "Articulo 1", description: "Descripcion del articulo numero 1"),
        Item(name: "Articulo 2", description: "Descripcion del articulo numero 2"),
        Item(name: "Articulo 3", description: "Descripcion del articulo numero 3"),
        Item(name: "Articulo 4", description: "Descripcion del articulo numero 4"),
        Item(name: "Articulo 5", description: "Descripcion del articulo numero 5"),
        Item(name: "Articulo 6", description: "Descripcion del articulo numero 6"),
        Item(name: "Articulo 7", description: "Descripcion del articulo numero 7"),
        Item(name: "Articulo 8", description: "Descripcion del articulo numero 8")
    ]
    
    var body: some View{
        NavigationView{
            List(items){ item in
                NavigationLink(destination: DetailView(item: item)){
                    Text(item.name)
                }
            }
        }
    }
}

struct ScrollContentView: View{
    var body: some View{
        ScrollView([.horizontal,.vertical]){
            VStack{
                Image("landscape")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .padding(EdgeInsets(top:10,leading: 10,bottom: 10, trailing: 10))
            }
        }
    }
}

struct TabContentView: View{
    @State private var selectedTab = 0
    var body: some View{
        TabView(selection: $selectedTab) {
            Image("logo")
                .resizable()
                .scaledToFit()
                .tag(0)
                .tabItem {
                    Label("Tab 1", systemImage: "house")
                }
            VStack{
                Circle()
                    .foregroundColor(.blue)
                    .frame(width: 100, height: 100)
                Text("This is Tab 2")
            }
            .tag(1)
            .tabItem {
                Label("Tab 2", systemImage: "heart")
            }
            Text("This is tab 3")
                .tag(2)
                .tabItem {
                    Label("Tab 3", systemImage: "map")
                }
        }
    }
}

struct AlertContentView: View{
    
    @State private var showAlert = false
    
    var body: some View{
        VStack{
            Button("Show Alert"){
                self.showAlert = true
            }
        }
        .alert(isPresented: $showAlert) {
            Alert(
            title: Text("Titulo"),
            message: Text("Aqui va el mensaje"),
            primaryButton: .destructive(
                Text("Delete"), action: {
                print("Se elimino este elemento")
            }),
            secondaryButton: .cancel(
                Text("Cancelar"))
            )
        }
    }
}


struct SheetContentView: View{
    
    @State var showSheet = false
    @State var selectedOption = 0
    
    var body: some View{
        Button(action:{
            self.showSheet = true
        }){
            Text("Showing Action Sheet")
        }
        .actionSheet(isPresented: $showSheet) {
            ActionSheet(title:
                            Text("Selecciona una opcion..."),
                        buttons: [
                            .default(Text("Option 1")){
                                self.selectedOption = 1
                                print("Se selecciono la primera opcion")
                            },
                            .default(Text("Option 2")){
                                self.selectedOption = 2
                                print("Se selecciono la segunda opcion")
                            },
                            .cancel()
                        ]
            )
        }
    }
}

struct DateContentView: View{
    
    @State var selectedDate = Date()
    
    var body: some View{
        VStack{
            DatePicker("Selecciona una fecha", selection: $selectedDate, displayedComponents: [.date])
                .datePickerStyle(.wheel)
                .labelsHidden()
                .onChange(of: selectedDate) { dateValue in
                    let formatter = DateFormatter()
                    formatter.dateFormat = "dd/MM/yyyy"
                    print(formatter.string(from: dateValue))
                }
            Text("La fecha seleccionada es: \(selectedDate)")
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        DateContentView()
    }
}
